# Currency Converter Website
This website is writen by HTML, Javscript and CSS. Responsive webpage design with Bootstrap.

## Additional Feature:
* Historical rates
* Dark/Light theme
* Searchable filtering currency selection
* Table cell mouse over color indication
* Mouse over table cell show exchange result
